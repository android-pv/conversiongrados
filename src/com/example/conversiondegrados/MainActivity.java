package com.example.conversiondegrados;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    int vC[] = new int[100];
    int vF[] = new int[100];
    int i=0;
    public void conversionCaF(View vista){
    	i++;
		EditText p1 =(EditText) findViewById(R.id.EditText01);
		int gradosC = Integer.parseInt("0"+p1.getText().toString());
		p1.setText(gradosC+"");
		EditText p2 = (EditText) findViewById(R.id.editText1);
		int gradosF = (int)(gradosC * 1.8) + 32;	//La formula de la conversion de C -> F
		p2.setText(gradosF+"");	
		vC[i]=gradosC;
		vF[i]=gradosF;
    }
    public void anterior(View vista){
		if(i>1){
        	EditText p1 =(EditText) findViewById(R.id.EditText01);
    		p1.setText(vC[i-1]+"");
    		EditText p2 = (EditText) findViewById(R.id.editText1);
    		p2.setText(vF[i-1]+"");
    		i--;
		}
    }
    public void conversionFaC(View vista){
    	i++;
		EditText p1 =(EditText) findViewById(R.id.editText1);
		int gradosF = Integer.parseInt("0"+p1.getText().toString());
		p1.setText(gradosF+"");
		EditText p2 = (EditText) findViewById(R.id.EditText01);
		int gradosC = (int)((gradosF - 32)/ 1.8);	//La formula de la conversion de C -> F
		p2.setText(gradosC+"");	
		vC[i]=gradosC;
		vF[i]=gradosF;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
