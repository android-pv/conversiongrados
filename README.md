# ConversionGrados

Convierte de grados Centigrados a
grados Fahrenheit

Ejercicios a resolver:

Nivel 1. Realizar una aplicaci�n Android para la conversi�n de grados Centigrados a
grados Fahrenheit. En el cuadro de abajo se muestra la f�rmula necesaria para realizar
la conversi�n.

Nivel 2. Agregar un bot�n para Inicializar el resultado y el dato introducido
anteriormente.

Nivel 3. Agregar un bot�n para implementar la conversi�n de Fahrenheit a
Cent�grados.


